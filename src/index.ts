import useHttpGet from "./hooks/useHttpGet";
import useHttpPost from "./hooks/useHttpPost";
import useHttpPut from "./hooks/useHttpPut";
import useHttpDelete from "./hooks/useHttpDelete";
import useClient from "./hooks/useClient";
import httpConfigurator from "./utils/config";

export {useHttpGet, useHttpPost, useHttpPut, useHttpDelete, useClient, httpConfigurator};
