import type {AxiosRequestConfig} from "axios";
import {Ref, ref, unref} from "vue";

export interface HttpConfigurator {
  config: Ref<AxiosRequestConfig>;
  addHeader(name: string, value: string): void;
  addParam(name: string, value: string): void;
  withAuthorization(token: string, type: string): void;
  withCredentials(value: boolean): void;
}

export const isHttpConfigurator = (obj: any | Ref<any>): obj is HttpConfigurator => {
  return (unref(obj) &&
    unref(obj).config &&
    unref(obj).addHeader &&
    unref(obj).addParam &&
    unref(obj).withCredentials &&
    unref(obj).withAuthorization) as boolean;
};

const httpConfigurator = (): HttpConfigurator => {
  const config: Ref<AxiosRequestConfig> = ref({});

  const addHeader = (name: string, value: string) => {
    if (config.value.headers) {
      config.value.headers[name] = value;
    } else config.value.headers = {[name]: value};
  };

  const addParam = (name: string, value: string) => {
    if (config.value.params) {
      config.value.params[name] = value;
    } else config.value.params = {[name]: value};
  };

  const withAuthorization = (token: string, type: string = "Bearer") => {
    if (config.value.headers) {
      config.value.headers.Authorization = `${type} ${token}`;
    } else config.value.headers = {Authorization: `${type} ${token}`};
    return this;
  };

  const withCredentials = (value: boolean) => {
    config.value.withCredentials = value;
    return this;
  };

  return {addHeader, addParam, withAuthorization, withCredentials, config};
};

export default httpConfigurator;
