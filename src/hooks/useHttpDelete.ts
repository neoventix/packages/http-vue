import {Ref, shallowReadonly, unref} from "vue";
import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {HttpDeleteHook, HttpGetHook, HttpState} from "./types";
import {createState} from "./utils";
import {HttpConfigurator, isHttpConfigurator} from "../utils/config";

/**
 * A hook to create reactive HTTP DELETE requests.
 *
 * @param uri The URI of the resource to request.
 * @param [config] Axios config to be added to the request.
 * @template T The response data type.
 * @returns {HttpGetHook} The state and the function to trigger the request.
 */
const useHttpDelete = <T>(
  uri: string | Ref<string>,
  config?: AxiosRequestConfig | HttpConfigurator,
): HttpDeleteHook<T> => {
  const state = createState<T>();

  /**
   * Makes the HTTP DELETE request
   * @param {string} [id] Optional ID that will be added as path param at the end of uri.
   */
  const request = (id?: string | Ref<string>) => {
    state.loading = true;

    axios
      .delete(`${unref(uri)}${id ? `/${unref(id)}` : ""}`, isHttpConfigurator(config) ? config.config.value : config)
      .then((res: AxiosResponse<T>) => {
        state.status = res.status;
        state.response = res.data;
      })
      .catch((err: AxiosError) => {
        state.error = err;
        if (err.response) state.status = err.response.status;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {
    state: shallowReadonly<HttpState<T>>(state),
    request,
  };
};

export default useHttpDelete;
