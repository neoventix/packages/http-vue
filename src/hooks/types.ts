import {ComputedRef, Ref} from "vue";

export interface HttpState<T> {
  response?: T;
  error?: Error;
  loading: boolean;
  status: number;
}

export interface HttpGetHook<T> {
  state: HttpState<T>;
  request: (id?: string | Ref<string>) => void;
}

export interface HttpPostHook<T, E> {
  state: HttpState<T>;
  request: (body: E | Ref<E>) => void;
}

export interface HttpDeleteHook<T> {
  state: HttpState<T>;
  request: (id?: string | Ref<string>) => void;
}

export interface HttpPutHook<T, E> {
  state: HttpState<T>;
  request: (body: E | Ref<E>, id?: string | Ref<string>) => void;
}

export interface HttpClient {
  loading: ComputedRef<boolean>;
  getResponse: Ref<any> | undefined;
  getStatus: Ref<number>;
  getLoading: Ref<boolean>;
  getError: Ref<Error | undefined> | undefined;
  get: (id?: string | Ref<string>) => void;
  postResponse: Ref<any> | undefined;
  postStatus: Ref<number>;
  postLoading: Ref<boolean>;
  postError: Ref<Error | undefined> | undefined;
  post: (body: any | Ref<any>) => void;
  putResponse: Ref<any> | undefined;
  putStatus: Ref<number>;
  putLoading: Ref<boolean>;
  putError: Ref<Error | undefined> | undefined;
  put: (body: any | Ref<any>, id?: string | Ref<string>) => void;
  removeResponse: Ref<any> | undefined;
  removeStatus: Ref<number>;
  removeLoading: Ref<boolean>;
  removeError: Ref<Error | undefined> | undefined;
  remove: (id?: string | Ref<string>) => void;
}
