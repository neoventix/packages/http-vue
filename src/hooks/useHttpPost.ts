import {reactive, Ref, shallowReadonly, unref} from "vue";
import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {HttpPostHook, HttpState} from "./types";
import {HttpConfigurator, isHttpConfigurator} from "../utils/config";

/**
 * A hook to create reactive HTTP POST requests.
 *
 * @param uri The URI of the resource to request.
 * @param [config] Axios config to be added to the request.
 * @template T The response data type.
 * @template E The request data type.
 * @returns {HttpPostHook} The state and the function to trigger the request.
 */
const useHttpPost = <T, E>(
  uri: string | Ref<string>,
  config?: AxiosRequestConfig | HttpConfigurator,
): HttpPostHook<T, E> => {
  const state: HttpState<T> = reactive({
    response: undefined,
    error: undefined,
    loading: false,
    status: -1,
  });

  /**
   * Makes the HTTP POST request
   * @param body The data that will be sent in the request.
   */
  const request = (body: E | Ref<E>) => {
    state.loading = true;
    axios
      .post(unref(uri), unref(body), isHttpConfigurator(config) ? config.config.value : config)
      .then((res: AxiosResponse<T>) => {
        state.status = res.status;
        state.response = res.data;
      })
      .catch((err) => {
        state.error = err;
        if (err.response) state.status = err.response.status;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {
    state: shallowReadonly<HttpState<T>>(state),
    request,
  };
};

export default useHttpPost;
