import {HttpClient} from "./types";
import {AxiosRequestConfig} from "axios";
import {HttpConfigurator} from "../utils/config";
import useHttpGet from "./useHttpGet";
import useHttpPost from "./useHttpPost";
import useHttpPut from "./useHttpPut";
import useHttpDelete from "./useHttpDelete";
import {computed, Ref, toRefs} from "vue";

/**
 * Creates a client that can be used to make reactive HTTP requests.
 *
 * @param {string | Ref<string>} uri The URI of the resource to be accessed.
 * @param {AxiosRequestConfig | HttpConfigurator} [config] The configuration options for the request.
 * @returns {HttpClient} A client that can be used to make HTTP requests.
 */
const useClient = (uri: string | Ref<string>, config?: AxiosRequestConfig | HttpConfigurator): HttpClient => {
  const getClient = useHttpGet(uri, config);
  const postClient = useHttpPost(uri, config);
  const putClient = useHttpPut(uri, config);
  const deleteClient = useHttpDelete(uri, config);

  const {response: getResponse, status: getStatus, loading: getLoading, error: getError} = toRefs(getClient.state);
  const get = getClient.request;

  const {response: postResponse, status: postStatus, loading: postLoading, error: postError} = toRefs(postClient.state);
  const post = postClient.request;

  const {response: putResponse, status: putStatus, loading: putLoading, error: putError} = toRefs(putClient.state);
  const put = putClient.request;

  const {
    response: removeResponse,
    status: removeStatus,
    loading: removeLoading,
    error: removeError,
  } = toRefs(deleteClient.state);
  const remove = deleteClient.request;

  const loading = computed(() => getLoading.value || postLoading.value || putLoading.value || removeLoading.value);

  return {
    loading,
    getResponse,
    getStatus,
    getLoading,
    getError,
    get,
    postResponse,
    postStatus,
    postLoading,
    postError,
    post,
    putResponse,
    putStatus,
    putLoading,
    putError,
    put,
    removeResponse,
    removeStatus,
    removeLoading,
    removeError,
    remove,
  };
};

export default useClient;
