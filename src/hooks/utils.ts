import {reactive} from "vue";
import {HttpState} from "./types";

export const createState = <T>(): HttpState<T> =>
  reactive({
    response: undefined,
    error: undefined,
    loading: false,
    status: -1,
  });
