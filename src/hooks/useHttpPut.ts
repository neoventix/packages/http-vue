import {Ref, shallowReadonly, unref} from "vue";
import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {HttpPutHook, HttpState} from "./types";
import {createState} from "./utils";
import {HttpConfigurator, isHttpConfigurator} from "../utils/config";

/**
 * A hook to create reactive HTTP PUT requests.
 *
 * @param uri The URI of the resource to request.
 * @param [config] Axios config to be added to the request.
 * @template T The response data type.
 * @template E The request data type.
 * @returns {HttpPutHook} The state and the function to trigger the request.
 */
const useHttpPut = <T, E>(
  uri: string | Ref<string>,
  config?: AxiosRequestConfig | HttpConfigurator,
): HttpPutHook<T, E> => {
  const state = createState<T>();

  /**
   * Makes the HTTP PUT request
   * @param body The data that will be sent in the request.
   * @param {string} [id] Optional ID that will be added as path param at the end of uri.
   */
  const request = (body: E | Ref<E>, id?: string | Ref<string>) => {
    state.loading = true;
    axios
      .put(
        `${unref(uri)}${id ? `/${unref(id)}` : ""}`,
        unref(body),
        isHttpConfigurator(config) ? config.config.value : config,
      )
      .then((res: AxiosResponse<T>) => {
        state.status = res.status;
        state.response = res.data;
      })
      .catch((err) => {
        state.error = err;
        if (err.response) state.status = err.response.status;
      })
      .finally(() => {
        state.loading = false;
      });
  };

  return {
    state: shallowReadonly<HttpState<T>>(state),
    request,
  };
};

export default useHttpPut;
